# Информация о проекте.
## Приложение "Task Manager"
Осуществляет вывод информации по введённой команде.

### Команды:
- help - доступные команды.
- about - информация о разработчике.
- version - версия приложения.
- info - информация о системе.
- exit - закрывает приложение.

# Стек
- Java 8.
- IntelliJ IDEA.
- Maven 3.

# Аппаратное обеспечение.
Процессор: 
- Intel Core 2 Quad и выше.
- Amd Athlon 64 и выше. 

ОЗУ: 2гб.    

Графический память: 512 Мб.

Переферийные устройства: клавиатура, мышь.          
       
# Программное обеспечение.
- JDK 1.8.
- Windows 7.
- Maven 3.

# Сборка jar файла
``` 
mvn clean package 
```
# Запуск приложения.
 ```
 java -jar target/taskmanager-1.0.0.jar help version about info
 ```

![](https://drive.google.com/uc?export=view&id=1OubBCAOo8Rm2jWXA2a-o_9cptjgGvBm2)

![](https://drive.google.com/uc?export=view&id=1ACSu_NTWojKnB-sGKqfJNwsnZk59OZxP)

### Ввод команд в консоль приложения

![](https://drive.google.com/uc?export=view&id=1JRcjkdH3hUNqgTvvyXh2aniQ_vn7Wmju)

# Разработчики.
**Имя**: Александр Карамышев.

**Телефон:** 8(800)-555-35-35.

**Email**: sanja_19.96@mail.ru.
