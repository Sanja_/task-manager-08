package ru.karamyshev.taskmanager.model;

import ru.karamyshev.taskmanager.constant.TerminalConst;

public class TerminalCommand implements TerminalConst {

    public static final TerminalCommand HELP = new TerminalCommand(
            CMD_HELP, ARG_HELP, DESCRIPTION_HELP
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            CMD_ABOUT, ARG_ABOUT, DESCRIPTION_ABOUT
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            CMD_VERSION, ARG_VERSION, DESCRIPTION_VERSION
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            CMD_EXIT, null, DESCRIPTION_EXIT
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            CMD_INFO, ARG_INFO, DESCRIPTION_INFO
    );

    private String name = "";

    private String arg = "";

    private String description = "";

    public TerminalCommand() {

    }

    public TerminalCommand(String name, String arg) {
        this.name = name;
        this.arg = arg;
    }

    public TerminalCommand(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);

        return result.toString();
    }
}