package ru.karamyshev.taskmanager;

import ru.karamyshev.taskmanager.constant.TerminalConst;
import ru.karamyshev.taskmanager.model.TerminalCommand;
import ru.karamyshev.taskmanager.util.NumberUtil;

import java.util.Scanner;

public class Application implements TerminalConst, NumberUtil {

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parsArgs(args);
        inputCommand();
    }

    private static void inputCommand() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            parsCommand(command);
        }
    }

    private static void parsCommand(final String args) {
        validateArgs(args);
        String[] command = args.trim().split("\\s+");
        for (String arg : command) { chooseResponsCommand(arg); }
    }

    private static void parsArgs(final String... args) {
        validateArgs(args);
        for (String arg : args) { chooseResponsArg(arg.trim()); }
    }

    private static void validateArgs(final String... args) {
        if (args != null || args.length > 0) { return; }
        System.out.println(COMMAND_ABSENT);
    }

    private static void chooseResponsArg(String arg) {
        switch (arg) {
            case ARG_ABOUT: showAbout(); break;
            case ARG_VERSION: showVersion(); break;
            case ARG_HELP: showHelp(); break;
            case ARG_INFO: showInfo(); break;
            default: System.out.println(ARGS_N_FOUND);
        }
    }

    private static void chooseResponsCommand(String command) {
        switch (command) {
            case CMD_ABOUT: showAbout(); break;
            case CMD_VERSION: showVersion(); break;
            case CMD_HELP: showHelp(); break;
            case CMD_INFO: showInfo(); break;
            case CMD_EXIT: exit(); break;
            default: System.out.println(COMMAND_N_FOUND);
        }
    }

    private static void showVersion() {
        System.out.println("\n [VERSION]");
        System.out.println("1.0.8");
    }

    private static void showAbout() {
        System.out.println("\n [ABOUT]");
        System.out.println("NAME: Alexander Karamyshev");
        System.out.println("EMAIL: sanja_19.96@mail.ru");
    }

    private static void showHelp() {
        System.out.println("\n [HELP]");
        System.out.println(TerminalCommand.ABOUT);
        System.out.println(TerminalCommand.VERSION);
        System.out.println(TerminalCommand.HELP);
        System.out.println(TerminalCommand.EXIT);
    }

    private static void showInfo() {
        System.out.println("\n [INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM (bytes): " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM (bytes): " + usedMemoryFormat);
    }

    private static void exit() { System.exit(0); }
}
