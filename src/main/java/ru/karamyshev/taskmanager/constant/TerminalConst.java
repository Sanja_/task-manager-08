package ru.karamyshev.taskmanager.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String ARG_HELP = "-h";

    String DESCRIPTION_HELP = "Display terminal commands.";

    String CMD_VERSION = "version";

    String ARG_VERSION = "-v";

    String DESCRIPTION_VERSION = "Show version info.";

    String CMD_ABOUT = "about";

    String ARG_ABOUT = "-a";

    String DESCRIPTION_ABOUT = "Show developer info.";

    String CMD_EXIT = "exit";

    String DESCRIPTION_EXIT = "Close application.";

    String CMD_INFO = "info";

    String ARG_INFO = "-i";

    String DESCRIPTION_INFO = "Display information about system.";

    String COMMAND_EMPTY = "\n Line is empty.";

    String COMMAND_N_FOUND = " \n Error! Command not found.";

    String ARGS_N_FOUND = " \n Error! Arguments not found.";

    String COMMAND_ABSENT = "\n Error! Command absent.";
}
